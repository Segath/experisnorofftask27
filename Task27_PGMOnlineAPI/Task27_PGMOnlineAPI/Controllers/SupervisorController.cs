﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task27_PGMOnlineAPI.Models;

namespace Task27_PGMOnlineAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {

        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetAllSupervisors()
        {
            return _context.Supervisors;
        }

        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetOneSupervisor(int id)
        {
            Supervisor sup = _context.Supervisors.Find(id);
            if(sup == null)
            {
                return NotFound();
            }
            return _context.Supervisors.Find(id);
        }

        [HttpPost]
        public ActionResult<Supervisor> CreateNewSupervisor(Supervisor sup)
        {
            _context.Supervisors.Add(sup);
            _context.SaveChanges();
            return CreatedAtAction("GetOneSupervisor", new Supervisor { Id = sup.Id }, sup);
        }

        [HttpDelete("{id}")]
        public ActionResult<Supervisor> DeleteSupervisor(int id)
        {
            Supervisor sup = _context.Supervisors.Find(id);
            if(sup == null)
            {
                return NotFound();
            }
            _context.Supervisors.Remove(sup);
            _context.SaveChanges();
            return sup;
        }

        [HttpPut("{id}")]
        public ActionResult<Supervisor> UpdateSupervisor(int id, Supervisor sup)
        {
            if(id != sup.Id)
            {
                return BadRequest();
            }

            var oldSup = _context.Supervisors.Find(id);

            if (oldSup == null)
            {
                return NotFound();
            }
            oldSup.Name = sup.Name;
            oldSup.Subject = sup.Subject;

            _context.Supervisors.Update(oldSup);
            _context.SaveChanges();
            return NoContent();
        }

    }
}